# frozen_string_liternal :true
require "rails_helper"
require "rspec_api_documentation/dsl"

resource "Status API" do
  # Headers which should be included in the request
  header "Accept", "application/vnd.api+json"

  get "/api/v1/ping" do
    example "Ping Status" do
      do_request
      expect(status).to eq 200
    end
  end
end
